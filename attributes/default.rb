default['java']['jdk_version'] = '8'
default['java']['install_flavor'] = 'oracle'
default['java']['oracle']['accept_oracle_download_terms'] = true

default['nginx']['install_method'] = 'source'
default['nginx']['default_site_enabled'] = false

default['tomcat-app']['nginx-http-port'] = 9080
default['tomcat-app']['nginx-https-port'] = 9443
