#
# Cookbook:: tomcat-app
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'java'

tomcat_install 'app' do
  version '8.0.36'
end

remote_file '/opt/tomcat_app/webapps/sample.war' do
  mode '0644'
  source 'https://tomcat.apache.org/tomcat-6.0-doc/appdev/sample/sample.war'
  checksum '89b33caa5bf4cfd235f060c396cb1a5acb2734a1366db325676f48c5f5ed92e5'
end

tomcat_service 'app' do
  action [:start, :enable]
end

bash 'generate SSL cert' do
  code <<-EOH
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/nginx-cert.key -out /etc/ssl/nginx-cert.crt -subj "/C=IE/ST=Dublin/L=Dublin/O=Company Name/OU=Org/CN=www.example.com"
  EOH
  not_if 'ls /etc/ssl/nginx-cert.key'
end

include_recipe 'chef_nginx'

nginx_site 'reverse_proxy' do
  name 'reverse_proxy'
  template 'reverse_proxy.erb'
  cookbook 'tomcat-app'
end
