# tomcat-app

A chef cookbook that performs the following tasks:

* Installs Java runtime
* Installs Tomcat
* Deploys a sample WAR application
* Generates a self signed SSL certificate
* Installs and configures nginx acting as a reverse proxy, with SSL termination.

Additionally the folder test/integration contains a few integration tests.

### How to test it with Vagrant
Before starting, install Berkshelf for Vagrant:
```
vagrant plugin install vagrant-berkshelf
```
Then you can run Vagrant:
```
cd chef/node-app
vagrant up
curl -skL http://127.0.0.1:9080/ # you can use either curl or a browser
vagrant destroy
```

### How to run integration tests
Before starting install the [Chef developer kit](https://downloads.chef.io/chefdk)

Then you can run the integration tests:
```
cd chef/node-app
kitchen converge
kitchen verify  # runs integration tests
# you can also browse http://127.0.0.1/9080
kitchen destroy
```
